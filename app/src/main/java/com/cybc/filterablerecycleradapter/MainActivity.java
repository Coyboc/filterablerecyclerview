package com.cybc.filterablerecycleradapter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.SearchView;

import com.cybc.filterablerecycleradapter.adapters.impl.NameSearchAdapter;
import com.cybc.filterablerecycleradapter.items.Name;
import com.cybc.filterablerecycleradapter.items.NameList;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SearchView.OnCloseListener, SearchView.OnQueryTextListener {

    private NameSearchAdapter filterableAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("Example");

        filterableAdapter = new NameSearchAdapter(new NameList());

        RecyclerView rv = (RecyclerView) findViewById(R.id.seachableRecyclerView);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(filterableAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
        return true;
    }

    @Override
    public boolean onClose() {
        filterableAdapter.filteringEnds();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        filterableAdapter.filter(newText);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }
}
