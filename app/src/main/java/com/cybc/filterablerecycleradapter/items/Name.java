package com.cybc.filterablerecycleradapter.items;

import com.cybc.filterablerecycleradapter.adapters.Filterable;

/**
 * Created by Kenny Seyffarth on 19.10.2016.
 */

public class Name implements Filterable{

    public final String name;

    public Name(String name) {this.name = name;}

    @Override
    public boolean isRelevant(String searchString) {
        String search = searchString.toLowerCase();
        return name.toLowerCase().contains(search);
    }
}
