package com.cybc.filterablerecycleradapter.items;

import java.util.ArrayList;

/**
 * Created by Kenny Seyffarth on 19.10.2016.
 */

public class NameList extends ArrayList<Name> {

    public NameList() {
        add(new Name("Kenny 'Moskito' Lary"));
        add(new Name("Steve 'Big Joe' Harris"));
        add(new Name("Felix da Turrent"));
        add(new Name("Sam the Fox"));
        add(new Name("Betti Amstrong"));
        add(new Name("Peter Ustinof"));
        add(new Name("Jennifer Richard"));
        add(new Name("Heidi da Klum"));
        add(new Name("Hans Fischer"));
        add(new Name("Garfield da Cat"));
        add(new Name("Dagobert Duck"));
        add(new Name("Applejack"));
        add(new Name("Twilight Sparkle"));
        add(new Name("Fluttershy"));
        add(new Name("Pinkie Pie"));
        add(new Name("Rainbow dash"));
        add(new Name("Rarity"));
        add(new Name("Spike"));
        add(new Name("Discord"));
        add(new Name("Trixie the Mighty"));
        add(new Name("Gilda"));
        add(new Name("Nightmare Moon"));
        add(new Name("Mozart"));
        add(new Name("Johan Sebastian Bach"));
        add(new Name("Lady Unnamed"));
        add(new Name("No 'Name Is a' Name!"));
        add(new Name("Lighter for Cigarettes"));
        add(new Name("ABC"));
        add(new Name("ABC DFG"));
        add(new Name("Foo Bar"));
        add(new Name("Hello World!"));
        add(new Name("Ranz pampe"));
        add(new Name("cybc"));
        add(new Name("Coyboc"));
        add(new Name("Hello Kitty for noobs"));
        add(new Name("Reality 'is' Down"));
        add(new Name("Dog Buster"));
        add(new Name("Betty van Holzdach"));
        add(new Name("Joana Geboren"));
        add(new Name("Justin Bieber"));
        add(new Name("Donald 'DA DUCK!' Trump"));
    }
}
