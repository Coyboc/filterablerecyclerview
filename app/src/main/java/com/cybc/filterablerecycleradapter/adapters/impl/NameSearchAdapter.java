package com.cybc.filterablerecycleradapter.adapters.impl;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cybc.filterablerecycleradapter.R;
import com.cybc.filterablerecycleradapter.adapters.FilterableAdapter;
import com.cybc.filterablerecycleradapter.items.Name;

import java.util.List;

/**
 * Created by Kenny Seyffarth on 19.10.2016.
 */

public class NameSearchAdapter extends FilterableAdapter<NameSearchAdapter.ViewHolder, Name> {

    private List<Name> nameList;

    public NameSearchAdapter(List<Name> nameList) {
        super(nameList);
        this.nameList = nameList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.name_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Name n = nameList.get(position);
        ((TextView) holder.itemView.findViewById(R.id.nameTextView)).setText(n.name);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
