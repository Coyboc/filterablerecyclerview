package com.cybc.filterablerecycleradapter.adapters;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kenny Seyffarth on 17.01.2016.
 */
public abstract class FilterableAdapter<VH extends RecyclerView.ViewHolder, T extends Filterable> extends RecyclerView.Adapter<VH> {

    private final List<T> originalList;
    private final List<T> items;

    public FilterableAdapter(List<T> originalList) {
        this.originalList = new ArrayList<>(originalList);
        this.items = new ArrayList<>(originalList);
    }

    protected T getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    protected int indexOf(T item) {
        return items.indexOf(item);
    }

    public List<T> getItems() {
        return items;
    }

    protected void remove(T item) {
        int oldPosition = items.indexOf(item);
        items.remove(item);
        notifyItemRemoved(oldPosition);
    }

    public void add(int index, T item) {
        items.add(index, item);
        notifyItemInserted(index);
    }

    private void filterRemovesAnimated(List<T> newItems) {
        for (int i = items.size() - 1; i >= 0; i--) {
            final T uiSongFile = items.get(i);
            if (!newItems.contains(uiSongFile)) {
                remove(uiSongFile);
            }
        }
    }

    private void filterAdditionsAnimated(List<T> newItems) {
        for (int i = 0; i < newItems.size(); i++) {
            final T uiSongFile = newItems.get(i);
            if (!items.contains(uiSongFile)) {
                add(i, uiSongFile);
            }
        }
    }

    private void filterMovementAnimated(List<T> newItems) {
        for (int toPosition = newItems.size() - 1; toPosition >= 0; toPosition--) {
            final T model = newItems.get(toPosition);
            final int fromPosition = items.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                move(fromPosition, toPosition);
            }
        }
    }

    private void move(int fromPosition, int toPosition) {
        final T item = items.remove(fromPosition);
        items.add(toPosition, item);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void filter(String newText) {
        if (newText.isEmpty()) {
            filterResult(originalList);
            return;
        }

        final List<T> filteredModelList = new ArrayList<>();

        for (T item : originalList) {
            if (item.isRelevant(newText)) {
                filteredModelList.add(item);
            }
        }
        filterResult(filteredModelList);
    }

    private void filterResult(List<T> items) {
        filterRemovesAnimated(items);
        filterAdditionsAnimated(items);
        filterMovementAnimated(items);
    }

    public void filteringEnds() {
        items.clear();
        items.addAll(originalList);
        notifyDataSetChanged();
    }
}
