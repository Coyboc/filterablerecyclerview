package com.cybc.filterablerecycleradapter.adapters;

/**
 * Created by Kenny Seyffarth on 22.04.2016.
 */
public interface Filterable {

    boolean isRelevant(String searchString);

}
